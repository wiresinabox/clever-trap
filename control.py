#Hello! This is the control script where everything gets executed.

#==== PUT IMPORTS HERE ====

import sensors as s
import calibrate as cali
import time
import os
import subprocess
from gpiozero import Servo
try:
    from picamera import PiCamera
    camera = PiCamera()
    camOk = True
except:
    camOk = False
#camera dir
cameraDir = './images'

#pins
gateOutPin = 19
gateInPin = 26

motorPin = 4


triggerMax = 1
triggerMin = 0.5



#Sensor Interfaces
yl40 = s.yl40()
servo = Servo(motorPin)
#I believe channel 0 is light, channel 1 is sensor, and channel 2 is an fsr and channel 3 is 
#either the potentiometer or the other fsr

#Get all channel values: yl40.checkAllChn()
#Get a sensor value    : val = yl40.read(chn)   chn is 0, 1, 2, or 3

#Listeners
outPIR = s.singlePIR(gateOutPin)
inPIR = s.singlePIR(gateInPin)
#to get the current status: isOn = outPIR.getCurrentStatus()
#to get the last ON time:   onTime = outPIR.getlastOnTime(), when it goes low to high
#to get the last OFF time:  offTime = outPIR.getlastOffTime(), when it goes high to low


outFRSfp = 'ain2frs.csv'
inFRSfp = 'ain3frs.csv'
tempfp = 'temp.csv'
lsrfp = 'lsr.csv'

outFRSfpExist = os.path.exists(cali.fp(outFRSfp))
inFRSfpExist = os.path.exists(cali.fp(inFRSfp))
tempfpExist = os.path.exists(cali.fp(tempfp))
lsrfpExist = os.path.exists(cali.fp(lsrfp))

#Check if the calibration stuff exist

caliDirExist = os.path.isdir(cali.caliDir)
if not outFRSfpExist or not caliDirExist:
    print('Please calibrate the outside facing FRS.')
    cali.runCli()
    cali.runCli()
if not inFRSfpExist or not caliDirExist:
    print('Please calibrate the inside facing FRS.')
    cali.runCli()
    cali.runCli()
if not tempfpExist or not caliDirExist:
    print('Please calibrate the Temperature Sensor.')
    cali.runCli()
    cali.runCli()
if not lsrfpExist or not caliDirExist:
    print('Please calibrate the Light Sensor.')
    cali.runCli()
    cali.runCli()

#For recording a single value
#O, I = FRS, o, i = PIR
#---------O-o---I-i---------------------------->time
#-------|-----x------|-------------------------In/Out recordings
#       theshold window aka 2 thresh
#When it starts recording again.
#---------O-o---I-i---------------------------->time
#-------------x------|-----------|-------------In/Out recordings


#Fit Interfaces here.

outFRSfit = cali.dataFit(cali.fp(outFRSfp))
inFRSfit = cali.dataFit(cali.fp(inFRSfp))
tempfit = cali.dataFit(cali.fp(tempfp))
lsrfit = cali.dataFit(cali.fp(lsrfp))
#use equivVal = outFRSfit.evaluateRawVal(sensorValue) to convert

#==== STATUS VARIABLES HERE =====
currentInside = 0 #Current Number of Squirrels Inside Box
totalPassesIn = 0 #Number of Passes through the outside
totalPassesOut = 0 #Number of Passes through the inside

lastActiveTime = 0
last4PeakTimes = [0,0,0,0] #OutFRS, InFRs, OutPIR, InPIR]. Once the next activation clears the last value
#its good to go.

#==== MAIN LOOP ====
lastOutFRStime = 0
lastInFRStime = 0

frsBufferLen = 10
outFRSbuffer = []
inFRSbuffer =[]

lastOutFRSmax = 0
lastOutFRSmaxTime = 0
lastOutFRSmaxTimeRec = 0
lastInFRSmax =0 
lastInFRSmaxTime =0 
lastInFRSmaxTimeRec =0 

#threshold
FRSvalthresh = 50 #Raw value
FRSlenthresh = 3 #Number of readings required before saying yes. This effectively 'smears' it
FRSresetTime = 1 #How long since not getting triggered in seconds is it ok to rest it.
activateThresh = 3 #+-seconds required to consider something has gone through
outFRSchn = 2
inFRSchn = 3
FRSdelay = 3 #In seconds, on the first threshold breach

tempInterval = 5 #seconds
lightInterval = 5 #seconds
lightRaw = 0
tempRaw = 0
outVal = 0
inVal = 0

trapArmed = True
trapThresh = 3 #No. Squirrels inside when trap is sprung
trapStartCountdown = 0
trapDelay = 5

servoDelay = 5

startTime = time.time()
def getElapsedTime(t):
    return t - startTime

def isIntervalOf(intervalTime, currentTime = False):
    if currentTime == False:
        currentTime = time.time()
    return (int(currentTime) % intervalTime) == 0

def dataPrint(rawTemp,fitTemp, rawLight,fitLight,rawOut,fitOut,rawIn,fitIn, pirOut, pirIn, frsOut, frsIn, totalIn):
    print(time.ctime(time.time()))
    print("TEMP:  {} - {} F".format(rawTemp, fitTemp))
    print("LIGHT: {} - {}".format(rawLight, fitLight))
    print("Out Pres.:  {} - {} g".format(rawOut, fitOut))
    print("In Pres. : {} - {} g ".format(rawIn, fitIn))
    print("PIR Out: {} PIR In: {}".format(pirOut, pirIn))
    print("FRS Out: {} FRS In: {}".format(frsOut, frsIn))
    print("Total In: {}".format(totalIn))


while True:
    try:
        #==== PUT CHECK ROUTINES HERE ====
        curTime = time.time() 
        servo.max()
        #Check Weights and time
        outFRSVal = yl40.read(outFRSchn)
        inFRSVal = yl40.read(inFRSchn)
        #

        #Always record buffer.
        outFRSbuffer.insert(0,outFRSVal)
        if len(outFRSbuffer) > frsBufferLen:
            outFRSbuffer.pop(-1)

        if all(val > FRSvalthresh for val in outFRSbuffer[0:FRSlenthresh]) and outFRSbuffer[FRSlenthresh+1] < FRSvalthresh:
            #Just started
            if curTime - lastOutFRSmaxTime > FRSresetTime:
                #It has been too long reset the max
                lastOutFRSmax=outFRSVal

        if all(val > FRSvalthresh for val in outFRSbuffer[0:FRSlenthresh]):
            # On State
            lastOutFRStime = curTime
                #write max here.
            if outFRSVal > lastOutFRSmax:
                lastOutFRSmax = outFRSVal
                lastOutFRSmaxTime = curTime

        if all(val < FRSvalthresh for val in outFRSbuffer[0:FRSlenthresh]): 
            if lastOutFRSmaxTime != lastOutFRSmaxTimeRec:
                #RECORD LAST MAXIMUM WEIGHT ON THE BOI. 
                lastOutFRSmaxTimeRec = lastOutFRSmaxTime
                pass
        
        
        inFRSbuffer.insert(0,inFRSVal)
        if len(inFRSbuffer) > frsBufferLen:
            inFRSbuffer.pop(-1)

        if all(val > FRSvalthresh for val in inFRSbuffer[0:FRSlenthresh]) and inFRSbuffer[FRSlenthresh+1] < FRSvalthresh:
            #Just started
            if curTime - lastInFRSmaxTime > FRSresetTime:
                #It has been too long reset the max
                lastInFRSmax=inFRSVal

        if all(val > FRSvalthresh for val in inFRSbuffer[0:FRSlenthresh]):
            # On State
            lastInFRStime = curTime
                #write max here.
            if inFRSVal > lastInFRSmax:
                lastInFRSmax = inFRSVal
                lastInFRSmaxTime = curTime

        if all(val < FRSvalthresh for val in inFRSbuffer[0:FRSlenthresh]): 
            if lastInFRSmaxTime != lastInFRSmaxTimeRec:
                #RECORD LAST MAXIMUM WEIGHT ON THE BOI. 
                lastInFRSmaxTimeRec = lastInFRSmaxTime
                pass
           
        #If the last FRS times are within a theshold, then something went through
        #Which one first determines an in or out recording
        #The PIRs can be also used to double check this. 

        lastOutPIRtime = outPIR.getLastOnTime()
        lastInPIRtime = inPIR.getLastOnTime()
        
        #booleans for all 4 sensors and if it is new
        isOutFRS = lastOutFRStime > (curTime - activateThresh) and lastOutFRStime < (curTime + activateThresh)
        isInFRS = lastInFRStime > curTime - activateThresh and lastInFRStime < curTime + activateThresh
        isOutPIR =outPIR.getCurrentStatus()  #lastOutPIRtime > curTime - activateThresh and lastOutPIRtime < curTime + activateThresh
        isInPIR = inPIR.getCurrentStatus() #lastInPIRtime > curTime - activateThresh and lastInPIRtime < curTime + activateThresh
        isNewActive = all(curTime - activateThresh > val for val in last4PeakTimes) #If the theshold window
        #has cleared all of the sensor values, then anything after them is new data.

        if isOutFRS and isInFRS and isOutPIR and isInPIR and isNewActive:
            #Something when through
            lastActiveTime = curTime
            last4PeakTimes = [lastOutFRStime, lastInFRStime, lastOutPIRtime, lastInPIRtime]
            if lastOutFRStime > lastInFRStime:
                #In to Out
                print("Something Went Out")
                totalPassesOut += 1
                currentInside -= 1
            else:
                #Out to In
                print("Something Went In")
                totalPassesIn += 1
                currentInside += 1
            
            #PUT THE RECORD DATA HERE, 

        
        #Get status of condition sensors 
        if isIntervalOf(lightInterval, curTime):
            lightRaw = yl40.read(0)
        if isIntervalOf(tempInterval, curTime):
            tempRaw = yl40.read(1)
        lightValFit = lsrfit.evaluateRawVal(lightRaw)
        tempValFit = tempfit.evaluateRawVal(tempRaw)
        outValFit = outFRSfit.evaluateRawVal(outFRSVal)
        inValFit = outFRSfit.evaluateRawVal(inFRSVal)

        #Door control here
        #Activate ONLY if trap is armed, the door is clear, threshold is met, and the delay time is done

        isDoorClear = not isOutFRS and not isInFRS 
        isTrapThreshMet = currentInside >= trapThresh

        #once the former two are met, start the count down

        if isDoorClear and isTrapThreshMet and trapStartCountdown == 0:
            trapStartCountdown = curTime
        elif (not isDoorClear or not isTrapThreshMet) and trapStartCountdown != 0:
            #otherwise, interrupt it
            trapStartCountdown = 0

        if trapStartCountdown != 0:
            isTrapDelayDone = curTime - (trapStartCountdown + trapDelay) <= 0

        if trapArmed and isDoorClear and isTrapThreshMet and isTrapDelayDone:
            servo.min()
            time.sleep(servoDelay)
            servo.max()
            if not os.path.isdir(cameraDir):
                os.mkdir(cameraDir)
            if camOk:
                camera.capture(cameraDir + '/image.jpg')
                subprocess.run('feh {}/image.jpg'.format(cameraDir), shell=True)

        #Print Status Updates
        print("\n")
        dataPrint(tempRaw, tempValFit, lightRaw,lightValFit, outFRSVal, outValFit, inFRSVal, inValFit, isOutPIR, isInPIR, isOutFRS, isInFRS, currentInside)

    except KeyboardInterrupt:
        #use controls
        #Commands:
        #door -> Activate Door
        #quit -> quit
        #spoof <val> -> change the number of squirrels currently in the box
        #camera -> Use the camera and display with feh!
        validCommands = 'door quit spoof camera'
        while True:
            command = input('> ')
            if command in validCommands:
                terms = command.split(' ')
                if terms[0] == 'door':
                    servo.min()
                    time.sleep(servoDelay)
                    servo.max()
                    
                if terms[0] == 'quit':
                    if camOk:
                        camera.close()
                    quit()
                if terms[0] == 'spoof':
                    try:
                        spoofVal = int(terms[1])
                        currentInside = spoofVal
                    except ValueError:
                        print('Unknown Value to Spoof')

                if terms[0] == 'camera':
                    if not os.path.isdir(cameraDir):
                        os.mkdir(cameraDir)
                    if camOk:
                        camera.capture(cameraDir + '/image.jpg')
                        subprocess.run('feh {}/image.jpg'.format(cameraDir), shell=True)
                break
            else:
                print("Unknown Command. Commands are:", validCommands)
    #=== PUT EFFECT ROUTINES HERE ====


    

    #=== PUT IO ROUTINES HERE ====
    
